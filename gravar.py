#abrir arquivo em modo edição
arquivo = open('arqText.txt', 'w')

#gravar no arquivo
arquivo.write('Curso Python \n')
arquivo.write('Aula Prática')
#encerra o arquivo após a utilização(Salva)
arquivo.close()

#leitura do arquivo texto

#abrir arquivo em modo leitura
leitura = open('arqText.txt', 'r')
#lendo arquivo
print(leitura.read())
#fechando arquivo
leitura.close()