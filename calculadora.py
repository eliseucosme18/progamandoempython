def adi(a, b):
    soma = a + b
    return soma

def sub(a, b):
    subtração = a - b
    return subtração

def mul(a, b):
    multiplicação = a * b
    return multiplicação

def div(a, b):
    divisão = a/b
    return divisão

def entrada():
    n1 = int(input('Digite o primeiro número: '))
    n2 = int(input('Digite o segundo número: '))
    return n1, n2


opção = 0

while opção != 5:

    opção = int(input('O que você quer fazer? \n 1 - Somar \n 2 - Subtrair \n 3 - Multiplicar \n 4 - Dividir \n 5 - sair \n'))
    if opção == 1:
        vlr1, vlr2 = entrada()
        print('Soma é: ',adi(vlr1, vlr2))
    if opção == 2:
        vlr1, vlr2 = entrada()
        print('A subtração é: ', sub(vlr1, vlr2))
    if opção == 3:
        vlr1, vlr2 = entrada()
        print('A multiplicação é: ', mul(vlr1, vlr2))
    if opção == 4:
        vlr1, vlr2 = entrada()
        print('A divisão é: ', div(vlr1, vlr2))

print('CALCULADORA FINALIZADA')
