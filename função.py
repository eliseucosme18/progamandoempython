#sintaxe de uma função
#def nome_da_função(parâmetros):
#       <instruções>
#   return "valor do retorno"
#def mensagem1():
   # print('Hello world')



#def mensagem2():
#   return 'hello word'


#mensagem1()
#mensagem2()

#mensagem2() não vai aparecer porque essa função é um retorn, retorn não imprime
#print(mensagem2())

def adicao(a, b):
    soma = a + b
    return soma

n1 = int(input('digite o primerio número: '))
n2 = int(input('digite o segundo número: '))

print('O resultado é: ',adicao(n1, n2))