#Iteradores são estruturas que permitem acesso a vários itens de uma coleção de elementos, de forma sequencial.
#sintaxe for: 
#       For{referência}in{sequência}:
#           {bloco de código}

#for n in range(10, 0, -1):
#    print(n)

#x = 1

#while x<=15:
#    print(x)
#    x=x+1

soma = 0
contador = 0

notas = float(input("Digite a nota \n"))

while notas > 0:
    soma = soma + notas
    contador = contador + 1
    notas = float(input("Digite a nota \n"))

media = soma / contador

print("valores digitados: ", contador)
print("soma dos valores: ", soma)
print("media dos valores: ", media)



